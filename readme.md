`yarn dev` で勝手に `.env.development` を使うテスト

# モチベ

`git clone` してすぐ `yarn && yarn dev` しても `.env` がないとエラるのは面倒い。

# memo

使い勝手は `dotenv-cli` の方が良いが依存している `dotenv` のバージョンが古い。
